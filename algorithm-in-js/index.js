function fibonacci(N) {
    N -= 2;
    var numbs = [1, 1]; // last two computed Fibonacci numbers
    var current = numbs[1];

    while (N > 0) {
        current = numbs[0] + numbs[1];
        numbs[0] = numbs[1]; // shift values
        numbs[1] = current;
        N--;
    }

    console.log('Your number is', numbs[1]);
}

fibonacci(53); // put here your Fibonacci number which you want to compute